package integration;

import Repository.BankAccountRepository;
import Repository.UserRepository;
import dto.BankAccountDto;
import model.BankAccount;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.SqlGroup;
import service.BankAccountService;

import javax.transaction.Transactional;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
@Transactional
@SqlGroup({
        @Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, scripts = "classpath:insert_TestData.sql")
})
public class BankAccountIT {
    @Autowired
    private BankAccountService bankAccountService;

    @Autowired
    BankAccountRepository bankAccountRepository;

    @Autowired
    UserRepository userRepository;

    @Test
    void addBankAccountTest()
    {
        BankAccountDto bankAccountDto = new BankAccountDto();
        bankAccountDto.setIban("DE 345 678 907");
        bankAccountDto.setDescription("new Iban from IT Test");
        Integer userId = 1;
        bankAccountDto.setUserId(userId);

        BankAccountDto createdBankAccount = bankAccountService.addBankAccount(bankAccountDto);

        assertThat(createdBankAccount.getId()).isNotNull();

    }

    @Test
    void deleteBankAccount()
    {
        assertThat(bankAccountService.desactivateBankAccount(1)).isTrue();
        BankAccount bankAccount = bankAccountRepository.findById(1).get();
        assertThat(bankAccount.isActif()).isFalse();
    }

    @Test
    void getBankAccountListForUser()
    {
        List<BankAccountDto> bankAccountDtoList = bankAccountService.getBankAccountListForUser(2);

        assertThat(bankAccountDtoList.size()).isEqualTo(2);
        assertThat(bankAccountDtoList.stream().allMatch(bankAccountDto -> bankAccountDto.isValid())).isTrue();
    }
}
